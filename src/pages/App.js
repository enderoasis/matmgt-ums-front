import React from "react";
import {
    Routes,
    Route,
    Navigate,
  } from "react-router-dom";

import Login from '../pages/Login';
import Home from '../pages/Home';
import Signup from '../pages/Signup';
import User from '../components/User';
import Settings from '../components/Settings';
import Organization from '../components/Organization';
import Job from '../components/Job';
import Permission from '../components/Permission';
import Role from '../components/Role';
import Main from '../components/Main';
import AuthService from "../services/auth.service";

import UserAdd from '../components/user/add/UserAdd';
import UserEdit from '../components/user/edit/UserEdit';


import OrganizationAdd from "../components/organization/add/OrganizationAdd";
import OrganizationEdit from "../components/organization/edit/OrganizationEdit";
import SettingsOrganization from "../components/organization/SettingsOrganization";

import Branch from "../components/branch/Branch";
import BranchAdd from "../components/branch/add/BranchAdd";
import BranchEdit from "../components/branch/edit/BranchEdit";
import BranchSettings from "../components/branch/BranchSettings";

const isLoggedIn = AuthService.isLoggedIn();

export default function App() {
    return (
      !isLoggedIn ? (
      <>
    <Routes>
      <Route path="/"  element={<Navigate replace to="/login" />} />   
      <Route path='/login' element={<Login/>} />
    </Routes>
    </>) : (
      <Routes>
          <Route path="/login"  element={<Navigate replace to="/" />} />   
          <Route index element={<Main />} />
          <Route path="/" element={<Home />}>
              <Route path="/settings" element={<Settings />} />
              <Route path="/organizations" element={<Organization />} />
              <Route path="/jobs" element={<Job />} />
              <Route path="/permissions" element={<Permission />} />
              <Route path="/roles" element={<Role />} />
              <Route path="/users/add" element={<UserAdd />} />
              <Route path="/users/edit" element={<UserEdit />} />
              <Route path="/organizations/add" element={<OrganizationAdd />} />
              <Route path="/organizations/edit" element={<OrganizationEdit />} />
              <Route path="/organizations/settings" element={<SettingsOrganization />} />
              <Route path="/users" element={<User />} />
              <Route path="/organization/branches" element={<Branch />} />
              <Route path="/organization/branches/add" element={<BranchAdd />} />
             <Route path="/organization/branch/edit" element={<BranchEdit />} />
              <Route path="/organization/branch/settings" element={<BranchSettings />} /> 
          </Route>
          <Route path="/organization/register" element={<Signup />}/>
      </Routes>
    )
  )
}