import React, { Component } from 'react'
import CompanyDetails from '../components/registration/CompanyDetails'
import BranchDetails from '../components/registration/BranchDetails'
import Confirmation from '../components/registration/Confirmation';
import Success from '../components/registration/Success';
import { Container,Grid, Paper, Box, Stepper, Step, StepLabel }  from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import CityService from "../services/city.service";


const steps = ['Organization Details', 'Branches details', 'Confirmation', 'Success'];
const theme = createTheme();

export default class Signup extends Component {

  state = {
    step: 1,
    org_email: '',
    org_name: '', 
    org_address: '',
    state: '',
    city: '',
    org_postal_code: '',
    org_primary_email: '',
    software: '',
    states: [],
    cities: [],
  }
  
  
  generateCitiesByState(state_id){
    CityService.getCitiesByState(state_id).then((res) => {
        this.setState({ cities: res.data.data });
        console.log(res.data.data);

    });
  }
  // go back to previous step
  prevStep = () => {
    const { step } = this.state;
    this.setState({ step: step - 1 });
  }

  // proceed to the next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  }

  // Handle fields change
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    if(input == 'state'){
        this.generateCitiesByState(e.target.value);
    }
  }

  render() {
    const { step } = this.state;
    const { org_email, org_name, org_address, state, city, org_postal_code, org_primary_email, software, cities, states } = this.state;
    const values = {  org_email, org_name, org_address, state, city, org_postal_code, org_primary_email, software, cities, states }

    switch(step) {
      case 1: 
        return (
      <ThemeProvider theme={theme}>
        <Grid container component="main" sx={{ height: '100vh' }}>
            <CssBaseline />
            <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
                <Box
                sx={{
                    my: 8,
                    mx: 4,
                    mt: 30,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                >
                    <Stepper activeStep={1}  sx={{ pt: 3, pb: 5 }}>
                        {steps.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                        ))}
                    </Stepper>
                    <CompanyDetails 
                        nextStep={ this.nextStep }
                        handleChange={ this.handleChange }
                        values={ values }
                    />
                </Box>
            </Grid>
            <Grid
                item
                xs={false}
                sm={4}
                md={9}
                sx={{
                backgroundImage: 'url(/images/cons-register.png)',
                backgroundRepeat: 'no-repeat',
                backgroundColor: (t) => t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                }} />
        </Grid>
        </ThemeProvider>
        )
      case 2: 
        return (
            <ThemeProvider theme={theme}>
            <CssBaseline />
            <Grid container component="main" sx={{ height: '100vh' }}>
                <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
                    <Box
                    sx={{
                        my: 8,
                        mx: 4,
                        mt: 30,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                    >
                        <Stepper activeStep={2} sx={{ pt: 3, pb: 5 }}>
                            {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                            ))}
                        </Stepper>
                        <BranchDetails 
                            prevStep={ this.prevStep }
                            nextStep={ this.nextStep }
                            handleChange={ this.handleChange }
                            values={ values }
                        />
                    </Box>
                </Grid>
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={9}
                    sx={{
                    backgroundImage: 'url(/images/branches-min.jpg)',
                    backgroundRepeat: 'no-repeat',
                    backgroundColor: (t) => t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                    backgroundSize: 'cover',
                    backgroundPosition: 'right',
                    }} />
            </Grid>
            </ThemeProvider>
        )
      case 3: 
          return (
            <ThemeProvider theme={theme}>
            <CssBaseline />
            <Grid container component="main" sx={{ height: '100vh' }}>
                <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
                    <Box
                    sx={{
                        my: 8,
                        mx: 4,
                        mt: 30,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                    >
                        <Stepper activeStep={3} sx={{ pt: 3, pb: 5 }}>
                            {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                            ))}
                        </Stepper>
                        <Confirmation 
                            prevStep={ this.prevStep }
                            nextStep={ this.nextStep }
                            values={ values }
                        />
                    </Box>
                </Grid>
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={9}
                    sx={{
                    backgroundImage: 'url(/images/cons-soft.webp)',
                    backgroundRepeat: 'no-repeat',
                    backgroundColor: (t) => t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                    backgroundSize: 'cover',
                    backgroundPosition: 'right',
                    }} />
            </Grid>
            </ThemeProvider>
          )
        case 4: 
          return (
            <ThemeProvider theme={theme}>
            <CssBaseline />
            <Grid container component="main" sx={{ height: '100vh' }}>
                <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
                    <Box
                    sx={{
                        my: 8,
                        mx: 4,
                        mt: 30,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                    >
                        <Stepper activeStep={4} sx={{ pt: 3, pb: 5 }}>
                            {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                            ))}
                        </Stepper>
                        <Success />
                    </Box>
                </Grid>
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={9}
                    sx={{
                    backgroundImage: 'url(/images/cons-register.png)',
                    backgroundRepeat: 'no-repeat',
                    backgroundColor: (t) => t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                    backgroundSize: 'cover',
                    backgroundPosition: 'right',
                    }} />
            </Grid>
            </ThemeProvider>
          )
      default: 
          // do nothing
    }
  }
}