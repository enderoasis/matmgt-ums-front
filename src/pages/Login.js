import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import axios from "axios";
import AuthService from "../services/auth.service";
import LinearProgress from '@mui/material/LinearProgress';

class SignInSide extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
        submitted: false,
    };

  }
  theme = createTheme();

  componentDidMount() {

 
}

  handleSubmit = (event) => {
  event.preventDefault();
  const data = new FormData(event.currentTarget);
  AuthService.login(data.get('email'), data.get('password')).then((res) => {
    if(res !== undefined){
        this.setState({
          submitted: true
      });
    }
});
};

  render() {
  const { submitted } = this.state;
 
  return (
      <ThemeProvider theme={this.theme}>
        <Grid container component="main" sx={{ height: '100vh' }}>
          <CssBaseline />
          <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
            <Box
              sx={{
                my: 8,
                mx: 4,
                mt: 30,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 2, bgcolor: 'secondary.main' }}>
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Box component="form" autocomplete="on" onSubmit={this.handleSubmit} sx={{ mt: 1 }}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="text"
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password" />
                <FormControlLabel
                  control={
                  <Checkbox value="remember" color="primary" />}
                  label="Remember me" />
                {submitted
                  ? <LinearProgress  sx={{ mt: 3 }}/>

                  : <><Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign In
                </Button>
                <Button    

                  type="submit"
                  fullWidth
                  color="success"
                  variant="contained"
                  onClick={() => {  window.location.href = "/organization/register"; }}
                  >
                    Sign up as organization
                  </Button></>
                }
                <Grid container>
                  <Grid item xs>

                  </Grid>
                  <Grid item  sx={{ mt: 3, mb: 2 }}>
                    <Link href="#" variant="body2">
                      Forgot password?
                    </Link>
                  </Grid>
                </Grid>
                <Typography variant="body2" color="text.secondary" sx={{ mt: 5 }} align="center">
                  {'Copyright © '}
                  <Link color="inherit" href="https://matmgt.com/">
                    Material Management
                  </Link>{' '}
                  {new Date().getFullYear()}
                  {'.'}
                </Typography>      
              </Box>
            </Box>
          </Grid>
          <Grid
            item
            xs={false}
            sm={4}
            md={9}
            sx={{
              backgroundImage: 'url(/images/cons-soft.webp)',
              backgroundRepeat: 'no-repeat',
              backgroundColor: (t) => t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
              backgroundSize: 'cover',
              backgroundPosition: 'right',
            }} />
        </Grid>
      </ThemeProvider>
  );
}
}

export default SignInSide;


 