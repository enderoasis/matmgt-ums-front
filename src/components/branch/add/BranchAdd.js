import { useLocation } from 'react-router-dom'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import { TextField, Button, Select, MenuItem, InputLabel, FormControl }  from '@mui/material';
import CityService from "../../../services/city.service";
import BranchService from "../../../services/branch.service";
import React, { useState, useEffect } from "react";

 
function BranchAdd () {
    const location = useLocation()
    const { org_id } = location.state
    const [branch, setBranch] = useState({
        GSID: '',
        org_id: org_id,
        name: '',
        address: '',
        state_id: '',
        city_name: '',
        postal_code: '',
        logo: ''
      });
    const [states, setStates] = useState([]);
    const inputFileRef = React.useRef();

    useEffect(() => {
      fetchStates()
    }, []);

    const fetchStates = () => {
            CityService.getStates().then((res) => {
                  setStates(res.data.data);
            });
     };

    const onBtnClick = () => {
        inputFileRef.current.click();
    }

    const handleClose = () => {
        window.location.href = "/organizations";
        // TO DO: Redirect to /org/branches and provide state with it!
    };

    const handleChange = e => {
        setBranch(prevState => {
            if(e.target.name == 'logo'){
                return{
                    ...prevState,
                    [e.target.name]: e.target.files[0],
               }    
            }
            else{
                return{
                    ...prevState,
                    [e.target.name]: e.target.value,
               }
            }
         })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        // Update the formData object
        formData.append('name',  branch.name);
        formData.append('GSID',  branch.GSID);
        formData.append('org_id',  branch.org_id);
        formData.append('address',  branch.address);
        formData.append('state_id',  branch.state_id);
        formData.append('city_name',  branch.city_name);
        formData.append('postal_code',  branch.postal_code);
        formData.append('logo',  branch.logo);

        console.log(formData.get('logo'));
        BranchService.create(formData);
        
    };


 return (
    <Dialog open={true} onClose={handleClose}>
    <DialogTitle>Add organization branch</DialogTitle>
    <DialogContent>
          <Box component="form" onSubmit={handleSubmit}>
                <input
                    id="org_id"
                    name="org_id"
                    value={org_id}
                    hidden
                />
            <FormControl fullWidth>                   
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="name"
                    label="Name"
                    name="name"
                    autoFocus
                    onChange={handleChange}

                />
            </FormControl>
            <FormControl fullWidth >                   
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="GSID"
                    label="GSID"
                    id="GSID"
                    type="number"
                    onChange={handleChange}

                    inputProps={{ min: "0", max: "9999999999"}} />
            </FormControl>
            <FormControl fullWidth>                   
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="address"
                    label="Address"
                    id="address"
                    onChange={handleChange}

                />
            </FormControl>
            <FormControl required fullWidth sx={{ mt: 2 }}>                   
                <InputLabel id="state-label">State</InputLabel>
                    <Select
                        required
                        fullWidth
                        label="State"
                        name="state_id"
                        onChange={handleChange}
                        labelId="state-label"
                        >
                        {  states.map((state) => (
                        <MenuItem value={state.id}>{state.name}</MenuItem>
                        ))}
                    </Select>
            </FormControl>
            <FormControl required fullWidth sx={{ mt: 2 }}>                   
            <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="city_name"
                    label="City"
                    id="city_name"
                    type="text"
                    onChange={handleChange}

            />
            </FormControl>
            <FormControl fullWidth>                   
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="postal_code"
                    label="Postal code"
                    id="postal_code"
                    type="number"
                    onChange={handleChange}

            />
            </FormControl>
            <FormControl fullWidth>                   
                     <Button variant="outlined"  onClick={onBtnClick}>
                        Upload logo
                    </Button> 
                    <input type="file" name="logo" ref={inputFileRef} accept="image/*" onChange={handleChange} hidden/>
            </FormControl>
            <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3 }}
                >
                Submit
            </Button>
         </Box>
         
        </DialogContent>
       <DialogActions>
    </DialogActions>
  </Dialog>
 )
}

export default BranchAdd;