
import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Divider,
    Grid,
    Tooltip,
    Container,
    FormControlLabel,
    FormGroup,
    Switch
  } from '@mui/material';
import BranchService from "../../services/branch.service";
import React, { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'


function BranchSettings() {
    const location = useLocation()
    const { branch } = location.state
    const [branchSettings, setBranchSettings] = useState({
      });
    
    
    const handleChange = (event) => {
        setBranchSettings(prevState =>{
          return{
               ...prevState,
               [event.target.name]: event.target.value,
          }
       })
    };

    const handleSubmit = () => {

    
    
    };
    
    return (       
            <Box
            component="main"
            sx={{
            flexGrow: 1,
            py: 5,
            }}
        >
            <Container maxWidth="lg">
                    <form>
                        <Card>
                            <CardHeader
                            subheader="The information can be edited"
                            title={branch.name + " - Settings"}
                            />
                            <Divider />
                            <CardContent>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <FormGroup>
                                            <Tooltip title="Force exclamation for non matching item codes. Forces the addition of a ! in front of a non-matching item code. On import Dexter Chaney will allow invalid items to be imported without a ! and subsequently prompt the user to add the item to the database" placement="top">
                                                 <FormControlLabel  control={<Switch defaultChecked />} label="Force exclamation for non matching item codes" />
                                            </Tooltip>
                                            <Tooltip title="When selecting this option, the system will exclude looking in Dexter Chaney for a valid UPC or Vendor Code. Items will never be reorganized as valid items." placement="top">
                                                <FormControlLabel  control={<Switch />} label="Do not import UPC code or Vendor Code" />
                                             </Tooltip>
                                            <FormControlLabel  control={<Switch />} label="Manually Load Jobs and Vendors" />
                                            <FormControlLabel  control={<Switch />} label="Remove Non Numeric Characters from PO #" />
                                            <FormControlLabel  control={<Switch />} label="Print Summary Report" />
                                            <FormControlLabel  control={<Switch />} label="Print all Orders at completion" />
                                            <FormControlLabel  control={<Switch />} label="Validate Job / Vendor for existing POs" />
                                            <FormControlLabel  control={<Switch />} label="Use Accounting Item Description (if Available)" />
                                            <FormControlLabel  control={<Switch />} label="Associate Item Code Selection with Item" />
                                            <FormControlLabel  control={<Switch />} label="Associate Cost Code with Item" />
                                            {/* Put radio button here */}
                                            <FormControlLabel  control={<Switch />} label="Use Vendor Specific Pricing" />
                                            <FormControlLabel  control={<Switch />} label="Take Precedence VendorCode Over UPC" />
                                            <FormControlLabel  control={<Switch />} label="On existing PO retain existing PO entries" />
                                            <FormControlLabel  control={<Switch />} label="Export the Price and UM as received" />
                                            <FormControlLabel  control={<Switch />} label="Export the Tax Code" />
                                            <FormControlLabel  control={<Switch />} label="Adjust Line Price / Extension round to 2 decimals" />
                                            <FormControlLabel  control={<Switch />} label="Export Order Image" />
                                            <FormControlLabel  control={<Switch />} label="Export GL Date as Today’s Date" />
                                            <FormControlLabel  control={<Switch />} label="Get Phase and Budget From Spectrum Remarks" />
                                            {/* Put radio button here */}
                                            <FormControlLabel  control={<Switch />} label="Associate GL with Job or Cost Center" />
                                            <FormControlLabel  control={<Switch />} label="Use Accounting Address if Blank from Supplier" />
                                            <FormControlLabel  control={<Switch />} label="Use Cost Center Table for Header AP Cost Center" />
                                            {/* Put radio button here */}
                                            <FormControlLabel  control={<Switch />} label="Use Tax Classes" />
                                            <FormControlLabel  control={<Switch />} label="Use 'Dispatch Status' criterion to fetch Work Orders" />
                                            {/* Put input here */}
                                            <FormControlLabel  control={<Switch />} label="Use 'Hold Status' criterion to fetch Work Orders" />
                                     </FormGroup>
                                </Grid>
                            </Grid>
                            </CardContent>
                            <Divider />
                            <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'flex-end',
                                p: 2
                            }}
                            >
                            <Button
                                color="primary"
                                variant="contained"
                                onClick={handleSubmit}
                            >
                                Save details
                            </Button>
                            </Box>
                        </Card>
                    </form>
            </Container>
        </Box>
      );
}

export default BranchSettings;