import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import EditIcon from '@mui/icons-material/Edit';
import Stack from '@mui/material/Stack';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Avatar from '@mui/material/Avatar';
import Container from '@mui/material/Container';
import {Link, NavigationType} from 'react-router-dom';
import SettingsIcon from '@mui/icons-material/Settings';
import React, { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'
import BranchService from "../../services/branch.service";
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { styled, alpha } from '@mui/material/styles';

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 5,
    marginTop: theme.spacing(1),
    minWidth: 90,
    color:
      theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity,
        ),
      },
    },
  },
}));

function Branch () {
    const location = useLocation()
    const { org } = location.state
    const [state, setState] = useState({
      org_id: org.id,
    });
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const [branches, setBranches] = useState([]);

    useEffect(() => {
        fetchBranches()
      }, []);
  
      const fetchBranches = () => {
        BranchService.index(state.org_id).then((res) => {
                setBranches(res.data.data);
              });
       };

      const handleSearch = (event) => {

          const keyword = event.target.value;
          
          if (keyword !== '') {
              const results = this.state.organizations.filter((org) => {
              return Object.keys(org).some(key =>     
                (typeof org[key] === 'string' && org[key].includes(keyword)) || (typeof org[key] === 'number') && org[key] === Number(keyword))
              });
    
              this.setState({
                organizations: results,
            });    
          }
          else{
            this.setState({
              organizations: this.state.nonFilteredOrgs,
          });   
          }
      }

      const handleClick = (event) => {
         setAnchorEl(event.currentTarget);
      };
      
      const handleClose = () => {
         setAnchorEl(null);
      };

      return (
          <Container maxWidth="lg">
            <Paper sx={{ maxWidth: '1200px', margin: 'auto', overflow: 'hidden' }}>
             <AppBar
               position="static"
               color="default"
               elevation={0}
               sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
             >
               <Toolbar>
                 <Grid container spacing={2} alignItems="center">
                   <Grid item>
                     <SearchIcon color="inherit" sx={{ display: 'block' }} />
                   </Grid>
                   <Grid item xs>
                     <TextField
                       fullWidth
                       onChange={handleSearch.bind(this)}
                       placeholder="Search by GSID, name, or address"
                       InputProps={{
                         disableUnderline: true,
                         sx: { fontSize: 'default' },
                       }}
                       variant="standard" />
                   </Grid>
                   <Grid item>
                        <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organization/branches/add" state={{ org_id: state.org_id }}>
                                  <Button variant="contained" sx={{ mr: 1 } } >
                                    Add branch
                                  </Button>
                        </Link>
                     <Tooltip onClick={fetchBranches} title="Reload">
                       <IconButton>
                         <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                       </IconButton>
                     </Tooltip>
                   </Grid>
                 </Grid>
               </Toolbar>
             </AppBar>
             <Table size="small">
               <TableHead>
                 <TableRow>
                   <TableCell>Logo</TableCell>
                   <TableCell>Name</TableCell>
                   <TableCell>GSID</TableCell>
                   <TableCell>Address</TableCell>
                   <TableCell>State</TableCell>
                   <TableCell>City</TableCell>
                   <TableCell>Postal code</TableCell>
                   <TableCell align="center">Actions</TableCell>
                 </TableRow>
               </TableHead>
               <TableBody>
               {branches.map((branch) => (
              <TableRow key={branch.id}>
                <TableCell><Avatar src={branch.logo_path}/></TableCell>
                <TableCell>{branch.name}</TableCell>
                <TableCell>{branch.GSID}</TableCell>
                <TableCell>{branch.address}</TableCell>
                <TableCell>{branch.state_name}</TableCell>
                <TableCell>{branch.city_name}</TableCell>
                <TableCell>{branch.postal_code}</TableCell>
                <TableCell>
                <Button
                  id="customized-button"
                  aria-controls={open ? 'customized-menu' : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? 'true' : undefined}
                  variant="outlined"
                  disableElevation
                  onClick={handleClick}
                  endIcon={<KeyboardArrowDownIcon />}
                >
                  Actions
                </Button>
                <StyledMenu
                  id="customized-menu"
                  MenuListProps={{
                    'aria-labelledby': 'customized-button',
                  }}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                >
                  <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organization/branch/edit" state={{ branch: branch }}>
                    <MenuItem  disableRipple>
                        <EditIcon />
                      Edit
                    </MenuItem>
                  </Link>
                  <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organization/branch/settings" state={{ branch: branch }}>
                    <MenuItem  disableRipple>
                        <SettingsIcon/>
                        Settings
                    </MenuItem>
                  </Link>
                </StyledMenu>
                </TableCell>
              </TableRow>
            ))}
               </TableBody>
             </Table>
           </Paper>
           </Container>
         )   
}

export default Branch;