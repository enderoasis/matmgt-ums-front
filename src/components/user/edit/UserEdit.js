import { useLocation } from 'react-router-dom'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import AuthService from "../../../services/auth.service";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import React, { useState } from "react";

function UserEdit () {
    const location = useLocation()
    const { user } = location.state
    const [state, setState] = useState({
      name: user.name,
      last_name: user.last_name,
      phone: user.phone,
      email: user.email,
    });

    const handleClose = () => {
        window.location.href = "/users";
    };

    const handleChange = e => {
      setState(prevState =>{
            return{
                 ...prevState,
                 [e.target.name]: e.target.value,
            }
         })
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        var object = {};
        data.forEach((value, key) => object[key] = value);
        var result = JSON.stringify(object);        
        console.log(result);
        AuthService.update(result,user.id);
    };

  return (
    <Dialog open={true} onClose={handleClose}>
    <DialogTitle>Edit user</DialogTitle>
    <DialogContent>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
            <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="name"
                  label="First name"
                  name="name"
                  value={state.name}
                  onChange={handleChange}

            />
            <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="last_name"
                  label="Last name"
                  id="last_name"
                  value={state.last_name}
                  onChange={handleChange}

            />
            <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="phone"
                  label="Phone number"
                  id="phone"
                  value={state.phone}
                  onChange={handleChange}

            />
            <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="email"
                  label="Email"
                  id="email"
                  value={state.email}
                  onChange={handleChange}

            />
            <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3 }}
                >
                Submit
            </Button>
         </Box>
         
        </DialogContent>
       <DialogActions>
    </DialogActions>
  </Dialog>
  )
}

export default UserEdit;