import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import AuthService from "../../../services/auth.service";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

class UserAdd extends  React.Component{

    constructor(
        
    ) {
        super();

    }
      
    
    componentDidMount() {
    }

    handleClose = () => {
        window.location.href = "/users";
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        var object = {};
        data.forEach((value, key) => object[key] = value);
        var result = JSON.stringify(object);
        AuthService.register(result);
    };

    render() {
        return(
            <Dialog open={true} onClose={this.handleClose}>
            <DialogTitle>Add user</DialogTitle>
            <DialogContent>
                  <Box component="form" onSubmit={this.handleSubmit} sx={{ mt: 1 }}>
                    <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="first_name"
                          label="First name"
                          name="first_name"
                          autoFocus
                    />
                    <TextField
                          margin="normal"
                          required
                          fullWidth
                          name="last_name"
                          label="Last name"
                          id="last_name"
                    
                    />
                    <TextField
                          type="tel"
                          margin="normal"
                          required
                          fullWidth
                          name="phone"
                          label="Phone number"
                          id="phone"
                    
                    />
                    <TextField
                          margin="normal"
                          required
                          fullWidth
                          name="email"
                          label="Email"
                          id="email"
                    
                    />
                    <Button
                        type="submit"
                        variant="contained"
                        sx={{ mt: 3 }}
                        >
                        Submit
                    </Button>
                 </Box>
                 
                </DialogContent>
               <DialogActions>
            </DialogActions>
          </Dialog>        )
    }
}

export default UserAdd;
