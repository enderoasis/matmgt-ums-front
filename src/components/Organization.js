import React, { useState, useEffect } from "react";
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import OrgService from "../services/org.service";
import CircularProgress from '@mui/material/CircularProgress';
import EditIcon from '@mui/icons-material/Edit';
import Stack from '@mui/material/Stack';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Avatar from '@mui/material/Avatar';
import Container from '@mui/material/Container';
import {Link, NavigationType} from 'react-router-dom';
import SettingsIcon from '@mui/icons-material/Settings';
import AltRouteIcon from '@mui/icons-material/AltRoute';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { styled, alpha } from '@mui/material/styles';


const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 5,
    marginTop: theme.spacing(1),
    minWidth: 90,
    color:
      theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity,
        ),
      },
    },
  },
}));


function Organization(){
    const [organizations, setOrganizations] = useState([]);
    const [loading, setLoading] = useState(false);
    const [nonFilteredOrgs, setnonFilteredOrgs] = useState([]);
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    useEffect(() => {
      fetchOrganizations()
    }, []);

    const fetchOrganizations = () => {
      OrgService.getOrgs().then((res) => {
        setOrganizations(res.data.data);
        setnonFilteredOrgs(res.data.data);
        setLoading(true);
    });
};

    const handleSearch = (event) => {

      const keyword = event.target.value;
      
      if (keyword !== '') {
          const results = organizations.filter((org) => {
          return Object.keys(org).some(key =>     
            (typeof org[key] === 'string' && org[key].includes(keyword)) || (typeof org[key] === 'number') && org[key] === Number(keyword))
          });

          setOrganizations(results);  
      }
      else{
        setOrganizations(nonFilteredOrgs);
      }
    }

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
      setAnchorEl(null);
    };

    return (
          <Container maxWidth="lg">

            <Paper sx={{ maxWidth: '1200px', margin: 'auto', overflow: 'hidden' }}>
             <AppBar
               position="static"
               color="default"
               elevation={0}
               sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
             >
               <Toolbar>
                 <Grid container spacing={2} alignItems="center">
                   <Grid item>
                     <SearchIcon color="inherit" sx={{ display: 'block' }} />
                   </Grid>
                   <Grid item xs>
                     <TextField
                       fullWidth
                       onChange={handleSearch.bind(this)}
                       placeholder="Search by GSID, name, or address"
                       InputProps={{
                         disableUnderline: true,
                         sx: { fontSize: 'default' },
                       }}
                       variant="standard" />
                   </Grid>
                   <Grid item>
                     <Button variant="contained" onClick={() => { window.location.href = "/organizations/add"; }} sx={{ mr: 1 } } >
                       Add organization
                     </Button>
                     <Tooltip title="Reload">
                       <IconButton>
                         <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                       </IconButton>
                     </Tooltip>
                   </Grid>
                 </Grid>
               </Toolbar>
             </AppBar>
             <Table size="small">
               <TableHead>
                 <TableRow>
                   <TableCell>Logo</TableCell>
                   <TableCell>Name</TableCell>
                   <TableCell>GSID</TableCell>
                   <TableCell>Created By</TableCell>
                   <TableCell>Address</TableCell>
                   <TableCell>State</TableCell>
                   <TableCell>City</TableCell>
                   <TableCell>Postal code</TableCell>
                   <TableCell align="center"></TableCell>
                 </TableRow>
               </TableHead>
               <TableBody>
               {organizations.map((org) => (
              <TableRow key={org.id}>
                <TableCell><Avatar src={org.logo_path}/></TableCell>
                <TableCell>{org.name}</TableCell>
                <TableCell>{org.GSID}</TableCell>
                <TableCell>{org.created_by}</TableCell>
                <TableCell>{org.address}</TableCell>
                <TableCell>{org.state_name}</TableCell>
                <TableCell>{org.city_name}</TableCell>
                <TableCell>{org.postal_code}</TableCell>
                <TableCell>
                <Button
                  id="customized-button"
                  aria-controls={open ? 'customized-menu' : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? 'true' : undefined}
                  variant="outlined"
                  disableElevation
                  onClick={handleClick}
                  endIcon={<KeyboardArrowDownIcon />}
                >
                  Actions
                </Button>
                <StyledMenu
                  id="customized-menu"
                  MenuListProps={{
                    'aria-labelledby': 'customized-button',
                  }}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                >
                  <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organizations/edit" state={{ org: org }}>
                    <MenuItem  disableRipple>
                        <EditIcon />
                      Edit
                    </MenuItem>
                  </Link>
                  <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organization/branches" state={{ org: org }}>
                    <MenuItem  disableRipple>
                        <AltRouteIcon/>
                      Branches
                    </MenuItem>
                  </Link>
                  <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/organizations/settings" state={{ org: org }}>
                    <MenuItem  disableRipple>
                        <SettingsIcon />
                      Settings
                    </MenuItem>
                  </Link>
                </StyledMenu>
                </TableCell>
              </TableRow>
            ))}
               </TableBody>
             </Table>
           </Paper>
           </Container>
    );
  
  }


export default Organization;
