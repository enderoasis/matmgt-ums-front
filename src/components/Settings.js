import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Checkbox,
    Divider,
    FormControlLabel,
    Container,

    Grid,
    Typography
  } from '@mui/material';
import * as React from 'react';
import AccountProfile from '../components/account/AccountProfile';
import AccountProfileDetails from '../components/account/AccountProfileDetails';
import UserService from "../services/user.service";

class Settings extends  React.Component{
    
    constructor() {
        super();

        this.state = {
            user: {},
         };
      }
  
    componentDidMount() {
      let user_id = JSON.parse(localStorage.getItem("user")).user.id;
      UserService.getExactUser(user_id).then((res) => {
        this.setState({
          user: res.data.data,
      });
    });

     }

    render() {

      return (
      <>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            py: 8
          }}
        >
            <Container maxWidth="lg">
              <Typography
                sx={{ mb: 3 }}
                variant="h4"
              >
                Account
              </Typography>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  lg={4}
                  md={6}
                  xs={12}
                >
                  <AccountProfile user={this.state.user} />
                </Grid>
                <Grid
                  item
                  lg={8}
                  md={6}
                  xs={12}
                >
                  <AccountProfileDetails user={this.state.user} />
                </Grid>
              </Grid>
            </Container>
        </Box>
    </>

      )
   }
}

export default Settings;
