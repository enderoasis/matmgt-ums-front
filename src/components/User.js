import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import UserService from "../services/user.service";
import CircularProgress from '@mui/material/CircularProgress';
import EditIcon from '@mui/icons-material/Edit';
import Stack from '@mui/material/Stack';
import {Link} from 'react-router-dom';


class User extends React.Component {

    constructor(props) {
      super(props);

      this.state = {
          users: [],
          DataisLoaded: false,
          showEditModal: false,
          nonFilteredUsers: []
      };
  
    }

  componentDidMount() {

    UserService.getUsers().then((res) => {
        this.setState({
          users: res.data.data,
          nonFilteredUsers: res.data.data,
          DataisLoaded: true
      });
    });
  }

  handleSearch(event){

    const keyword = event.target.value;
    
    if (keyword !== '') {
        const results = this.state.users.filter((user) => {
          return Object.keys(user).some(key =>     
            (typeof user[key] === 'string' && user[key].includes(keyword)) || (typeof user[key] === 'number') && user[key] === Number(keyword))
      });
        this.setState({
          users: results,
      });    
    }
    else{
      this.setState({
        users: this.state.nonFilteredUsers,
    });   
    }
  }
 
 
  render() {
    const { DataisLoaded, users } = this.state;
    
    if (!DataisLoaded) return <center><CircularProgress /></center>    
    return (
       <Paper sx={{ maxWidth: '1200px', margin: 'auto', overflow: 'hidden' }}>
        <AppBar
          position="static"
          color="default"
          elevation={0}
          sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
        >
          <Toolbar>
            <Grid container spacing={2} alignItems="center">
              <Grid item>
                <SearchIcon color="inherit" sx={{ display: 'block' }} />
              </Grid>
              <Grid item xs>
                <TextField
                  fullWidth
                  placeholder="Search by email address, phone number, or user UID"
                  InputProps={{
                    disableUnderline: true,
                    sx: { fontSize: 'default' },
                  }}
                  onChange={this.handleSearch.bind(this)}
                  variant="standard" />
              </Grid>
              <Grid item>
                <Button variant="contained" onClick={() => { window.location.href = "/users/add"; }} sx={{ mr: 1 } } >
                  Add user
                </Button>
                <Tooltip title="Reload">
                  <IconButton>
                    <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                  </IconButton>
                </Tooltip>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Last name</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>Email</TableCell>
              <TableCell align="left">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user.id}>
                <TableCell>{user.id}</TableCell>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.last_name}</TableCell>
                <TableCell>{user.phone}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>
                  <Stack direction="row" spacing={0.5}>
                    <IconButton aria-label="Edit">
                    <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/users/edit" state={{ user: user }}>
                      <EditIcon/>
                    </Link>
                    </IconButton>
                    <IconButton aria-label="Delete" onClick={() => { this.deleteHandle(user); } }>
                      {/* <DeleteIcon /> */}
                    </IconButton>
                  </Stack>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
     
    );
  }
}

export default User;
