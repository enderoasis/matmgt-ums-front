import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import AuthService from "../services/auth.service";
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import UserService from "../services/user.service";
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}));

function Header(props) {
  const { onDrawerToggle } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [user, setUser] = React.useState({    
      name: '',
      avatar: ''
    }
  );

  const logged_in = AuthService.isLoggedIn();
  const storaged_user = JSON.parse(localStorage.getItem("user"));

  
  useEffect(() => {
    fetchUserData()
  }, []);

  const fetchUserData = () => {
        UserService.getExactUser(storaged_user.user.id).then((res) => {
          setUser(res.data.data)
      });
   };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
 
  

  return (
    <React.Fragment>
      <AppBar color="primary" position="sticky" elevation={2}>
        <Toolbar>      
          <Grid container spacing={1} alignItems="center">
            <Grid sx={{ display: { sm: 'none', xs: 'block' } }} item>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={onDrawerToggle}
                edge="start"
              >
                <MenuIcon />
              </IconButton>
            </Grid>
            <Grid item xs />
            {logged_in && (
            <div>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                  <Typography fontWeight={600} mr={1.5}  fontSize={14}>
                  Hello, {user.name}
                  </Typography>
                  <StyledBadge
                    overlap="circular"
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    variant="dot"
                    >
                    <Avatar
                    src={user.avatar}
                    sx={{
                      height: 30,
                      mb: 0,
                      width: 30
                  }}/>
                  </StyledBadge>
              </IconButton>
              <Menu
                sx={{mt:3.5}}
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem  onClick={exit}>Logout</MenuItem>
              </Menu>
            </div>
          )}
          </Grid>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

function exit(){
  AuthService.logout();
}

Header.propTypes = {
  onDrawerToggle: PropTypes.func.isRequired,
};

export default Header;