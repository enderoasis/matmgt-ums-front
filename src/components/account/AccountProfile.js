import {
    Avatar,
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    Divider,
    Typography
  } from '@mui/material';
  import React, {useState} from 'react';
  import UserService from "../../services/user.service";

  function AccountProfile(props){
    
    const inputFileRef = React.useRef();

    const onFileUpload = (avatar_file) => {
      // Create an object of formData
      const formData = new FormData();
      // Update the formData object
      formData.append('avatar', avatar_file);
      formData.append('user_id', props.user.id);
      UserService.uploadUserAvatar(formData);
      };

    const onFileChangeCapture = event => {
      /*Selected files data can be collected here.*/
      onFileUpload(event.target.files[0]);

    };


    const onBtnClick = () => {
      inputFileRef.current.click();
    };
    
      return(
        <Card>
        <CardContent>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              flexDirection: 'column'
            }}
          >
            <input type="file" ref={inputFileRef} accept="image/*" onChange={onFileChangeCapture} hidden/>
            <Avatar
              src={props.user.avatar}
              sx={{
                height: 64,
                mb: 2,
                width: 64
              }}
            />
             
            <Typography
              color="textPrimary"
              gutterBottom
              variant="h5"  
            >
              {props.user.name}
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              {`${props.user.email}`}
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              {props.user.phone}
            </Typography>
          </Box>
        </CardContent>
        <Divider />
        <CardActions>
          
          <Button
            color="primary"
            fullWidth
            variant="text"
            onClick={onBtnClick}>
            Upload picture
          </Button>
         
        </CardActions>
      </Card>
      )
    
  }
 
  
  export default AccountProfile;
