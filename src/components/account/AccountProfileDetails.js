import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField
} from '@mui/material';
import UserService from "../../services/user.service";



function AccountProfileDetails () {


  const [user, setUser] = useState({
    name: '',
    last_name: '',
    email: '',
    phone: '',
  });


  useEffect(() => {
    fetchCurrentUserData()
  }, []);

  const fetchCurrentUserData = () => {
      let user_id = JSON.parse(localStorage.getItem("user")).user.id;
      UserService.getExactUser(user_id).then((res) => {
        setUser(res.data.data);
    });
  };

  const handleChange = (event) => {
    setUser(prevState =>{
      return{
           ...prevState,
           [event.target.name]: event.target.value,
      }
   })
  };


  const handleSubmit = () => {
    UserService.updateUser(user,user.id);
    };

  return (
    <form>
      <Card>
        <CardHeader
          subheader="The information can be edited"
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                helperText="Please specify the first name"
                label="First name"
                name="name"
                onChange={handleChange}
                required
                value={user.name}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Last name"
                name="last_name"
                onChange={handleChange}
                required
                value={user.last_name}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                name="email"
                onChange={handleChange}
                required
                value={user.email}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Phone Number"
                name="phone"
                onChange={handleChange}
                type="text"
                value={user.phone == null ? '' : user.phone}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            p: 2
          }}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
          >
            Save details
          </Button>
        </Box>
      </Card>
    </form>
  );
};
export default AccountProfileDetails;
