import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import { TextField, Button, Select, MenuItem, InputLabel, FormControl }  from '@mui/material';
import CityService from "../../../services/city.service";
import OrgService from "../../../services/org.service";

class OrganizationAdd extends React.Component{

    constructor(
        
    ) {
        super();
        this.state = {
            states: [],
            name: '',
            GSID: '',
            address: '',
            state_id: '',
            city_name: '',
            postal_code: '',
            logo: ''
        };
        this.inputFileRef = React.createRef();

    }
      
    
    componentDidMount() {
        CityService.getStates().then((res) => {
            this.setState({ states: res.data.data });
        });
    }

    handleClose = () => {
        window.location.href = "/organizations";
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData();
        // Update the formData object
        formData.append('name',  this.state.name);
        formData.append('GSID',  this.state.GSID);
        formData.append('address',  this.state.address);
        formData.append('state_id',  this.state.state_id);
        formData.append('city_name',  this.state.city_name);
        formData.append('postal_code',  this.state.postal_code);
        formData.append('logo',  this.state.logo);

        console.log(formData);
        OrgService.create(formData);
    };

    handleChange = input => event => {
        if(input == 'logo'){
            this.setState({'logo': event.target.files[0]}, () => {
                console.log(this.state);
                });
        }
        else{
            this.setState({ [input]: event.target.value  } , () => {
                console.log(this.state);
                });
        }
    }

    onBtnClick = () => {
        this.inputFileRef.current.click();
    }
    
    render() {
        const { states } = this.state;

        return(
            <Dialog open={true} onClose={this.handleClose}>
            <DialogTitle>Add customer organization</DialogTitle>
            <DialogContent>
                  <Box component="form" onSubmit={this.handleSubmit}>
                    <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Name"
                            name="name"
                            autoFocus
                            onChange={this.handleChange('name')}
                        />
                    </FormControl>
                    <FormControl fullWidth >                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="GSID"
                            label="GSID"
                            id="GSID"
                            type="number"
                            onChange={this.handleChange('GSID')}
                            inputProps={{ min: "0", max: "9999999999"}} />
                    </FormControl>
                    <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="address"
                            label="Address"
                            id="address"
                            onChange={this.handleChange('address')}
                        />
                    </FormControl>
                    <FormControl required fullWidth sx={{ mt: 2 }}>                   
                        <InputLabel id="state-label">State</InputLabel>
                            <Select
                                required
                                fullWidth
                                label="State"
                                name="state_id"
                                labelId="state-label"
                                onChange={this.handleChange('state_id')}
                                    >
                                {  states.map((state) => (
                                <MenuItem value={state.id}>{state.name}</MenuItem>
                                ))}
                            </Select>
                    </FormControl>
                    <FormControl required fullWidth sx={{ mt: 2 }}>                   
                    <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="city_name"
                            label="City"
                            id="city_name"
                            type="text"
                            onChange={this.handleChange('city_name')}
                    />
                    </FormControl>
                    <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="postal_code"
                            label="Postal code"
                            id="postal_code"
                            type="number"
                            onChange={this.handleChange('postal_code')}
                    />
                    <Button variant="outlined"  onClick={this.onBtnClick}>
                        Upload logo
                    </Button> 
                    <input type="file" name="logo" ref={this.inputFileRef} accept="image/*" onChange={this.handleChange('logo')} hidden/>
                    </FormControl>
                    <Button
                        type="submit"
                        variant="contained"
                        sx={{ mt: 3 }}
                        >
                        Submit
                    </Button>
                 </Box>
                 
                </DialogContent>
               <DialogActions>
            </DialogActions>
          </Dialog>        )
    }
}

export default OrganizationAdd;
