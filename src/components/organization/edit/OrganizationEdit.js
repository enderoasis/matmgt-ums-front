import { useLocation } from 'react-router-dom'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import { TextField, Button, Select, MenuItem, InputLabel, FormControl }  from '@mui/material';
import CityService from "../../../services/city.service";
import OrgService from "../../../services/org.service";
import React, { useState, useEffect } from "react";

 
function OrganizationEdit () {
    const location = useLocation()
    const { org } = location.state
    const [state, setState] = useState({
      GSID: org.GSID,
      name: org.name,
      address: org.address,
      state_id: org.state_id,
      city_name: org.city_name,
      postal_code: org.postal_code,
      logo: org.logo_path
    });
    const [states, setStates] = useState([]);
    const [cities, setCities] = useState([]);
    const inputFileRef = React.useRef();

    console.log(state)

    useEffect(() => {
      fetchStates()
    }, []);

    const fetchStates = () => {
            CityService.getStates().then((res) => {
                  setStates(res.data.data);
            });
     };

    

    const handleChange = e => {

      setState(prevState => {
            if(e.target.name == 'logo'){
                return{
                    ...prevState,
                    [e.target.name]: e.target.files[0],
               }    
            }
            else{
                return{
                    ...prevState,
                    [e.target.name]: e.target.value,
               }
            }
         })
    };

    const handleClose = () => {
        window.location.href = "/organizations";
    };

    const onBtnClick = () => {
        inputFileRef.current.click();

    }

    const handleSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData();
        // Update the formData object
        formData.append('_method', 'patch')
        formData.append('name',  state.name);
        formData.append('GSID',  state.GSID);
        formData.append('address',  state.address);
        formData.append('state_id',  state.state_id);
        formData.append('city_name',  state.city_name);
        formData.append('postal_code',  state.postal_code);
        formData.append('logo',  state.logo);

        console.log(state);
        OrgService.update(formData,org.id);

    };


  return (
    <Dialog open={true} onClose={handleClose}>
    <DialogTitle>Edit customer organization</DialogTitle>
    <DialogContent>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
                     <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Name"
                            name="name"
                            autoFocus
                            value={state.name}
                            onChange={handleChange}
                        />
                    </FormControl>
                    <FormControl fullWidth >                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="GSID"
                            label="GSID"
                            id="GSID"
                            type="number"
                            value={state.GSID}
                            onChange={handleChange}
                            inputProps={{ min: "0", max: "9999999999"}} />
                    </FormControl>
                    <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="address"
                            label="Address"
                            id="address"
                            value={state.address}
                            onChange={handleChange}

                        />
                    </FormControl>
                    <FormControl required fullWidth sx={{ mt: 2 }}>                   
                        <InputLabel id="state-label">State</InputLabel>
                            <Select
                                required
                                fullWidth
                                label="State"
                                name="state_id"
                                labelId="state-label"
                                value={state.state_id}
                                onChange={handleChange}
                                >
                                {  states.map((state) => (
                                <MenuItem value={state.id}>{state.name}</MenuItem>
                                ))}
                            </Select>
                    </FormControl>
                    <FormControl required fullWidth sx={{ mt: 2 }}>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="city_name"
                            label="City"
                            id="city_name"
                            type="text"
                            value={state.city_name}
                            onChange={handleChange}
                    />
                    </FormControl>
                    <FormControl fullWidth>                   
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="postal_code"
                            label="Postal code"
                            id="postal_code"
                            type="number"
                            value={state.postal_code}
                            onChange={handleChange}
                    />
                    </FormControl>
                    <FormControl fullWidth>                   
                        <Button variant="outlined"  onClick={onBtnClick}>
                            Upload logo
                        </Button> 
                        <input type="file" name="logo" ref={inputFileRef} accept="image/*" onChange={handleChange} hidden/>
                    </FormControl>

            <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3 }}
                >
                Submit
            </Button>
         </Box>
         
        </DialogContent>
       <DialogActions>
    </DialogActions>
  </Dialog>
  )
}

export default OrganizationEdit;