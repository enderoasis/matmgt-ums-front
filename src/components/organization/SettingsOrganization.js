import {
    Button,
    Card,
    CardContent,
    CardHeader,
    TextField,
    Divider,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Grid,
    Typography,
    TextareaAutosize,
    RadioGroup,
    FormControlLabel,
    Radio,
    FormLabel
  } from '@mui/material';
import SettingsService from "../../services/settings.service";
import React, { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import {Link, NavigationType} from 'react-router-dom';

function SettingsOrganization () {

    const location = useLocation()
    const { org } = location.state
    const [settings, setSettings] = useState({
        acc_software_id: '',
        acc_system_connection_string: '',
        acc_software_version: '',
        agent_version: '',
        agent_autoupdate: 0
      });

    const [softwares, setSoftwares] = useState([]);

    useEffect(() => {
        fetchSoftwares()
        fetchCurrentOrgSettings()
      }, []);
  
    const fetchSoftwares = () => {
        SettingsService.getSoftwares().then((res) => {
            setSoftwares(res.data.data);
        });
    };
  
    const fetchCurrentOrgSettings = () => {
        SettingsService.getOrgSettings(org.id).then((res) => {
            setSettings(res.data.data);
        });
    };

    const handleChange = e => {

        setSettings(prevState => {
              return{
                   ...prevState,
                   [e.target.name]: e.target.value,
              }
           })
      };
  
      const handleSubmit = (event) => {
          event.preventDefault();
          const data = new FormData(event.currentTarget);
          var object = {};
          data.forEach((value, key) => object[key] = value);
          var result = JSON.stringify(object);
          console.log(result);

          SettingsService.updateOrgSettings(result,org.id);
  
      };



      return(
        <Card sx={{ maxWidth: '1200px', margin: 'auto', overflow: 'hidden' }}>
            
                  <CardHeader title={org.name + " - Settings"} />
                  <Divider />
                  <CardContent>
                      <form onSubmit={handleSubmit}>
                          <Grid container spacing={6} wrap="wrap">
                              <Grid item md={4} sm={6} sx={{ display: 'flex', flexDirection: 'column' }} xs={12}>
                                  <Typography
                                      color="textPrimary"
                                      gutterBottom
                                      variant="h6"
                                  >
                                      Accounting software package
                                  </Typography>
                                  <FormControl fullWidth sx={{ mt: 2 }}>
                                      <InputLabel id="acc_software-label">Accounting software</InputLabel>
                                      <Select
                                          fullWidth
                                          label="Accounting software"
                                          name="acc_software_id"
                                          value={settings.acc_software_id}
                                          labelid="acc_software-label"
                                          onChange={handleChange}
                                      >
                                          {softwares.map((softwares) => (
                                              <MenuItem value={softwares.id}>{softwares.name}</MenuItem>
                                          ))}
                                      </Select>
                                  </FormControl>
                                  <FormControl sx={{ width: '15ch' }}>
                                      <TextField
                                          margin="normal"
                                          name="acc_software_version"
                                          label="Software version"
                                          id="acc_software_version"
                                          value={settings.acc_software_version}
                                          onChange={handleChange} />
                                  </FormControl>
                                  <Typography
                                      color="textPrimary"
                                      sx={{ mt: 5 }}
                                      variant="p">
                                      Accounting DB connection
                                  </Typography>
                                  <FormControl fullWidth sx={{ mt: 2 }}>
                                      <TextareaAutosize
                                          minRows={2}
                                          name="acc_system_connection_string"
                                          value={settings.acc_system_connection_string}
                                          onChange={handleChange}
                                          id="acc_system_connection_string"
                                          style={{ width: 400 }} />
                                  </FormControl>
                                  <FormControl sx={{ mt: 2, width: '15ch' }}>
                                      <TextField
                                          margin="normal"
                                          name="agent_version"
                                          label="Agent version"
                                          id="agent_version"
                                          value={settings.agent_version}
                                          onChange={handleChange} />
                                  </FormControl>
                                  <FormControl sx={{ mt: 2 }}>
                                      <FormLabel id="demo-radio-buttons-group-label">Agent autoupdate</FormLabel>
                                      <RadioGroup
                                          labelid="agent_autoupdate-label"
                                          name="agent_autoupdate"
                                          value={settings.agent_autoupdate}
                                          onChange={handleChange}>
                                          <FormControlLabel value="0" control={<Radio />} label="Off" />
                                          <FormControlLabel value="1" control={<Radio />} label="On" />
                                      </RadioGroup>
                                  </FormControl>
                              </Grid>
                          </Grid>
                          <Button
                              type="submit"
                              variant="contained"
                              sx={{ mt: 5 }}>
                              Submit
                          </Button>
                      </form>
                  </CardContent>
                  <Divider />
              </Card>
      );
}
    
export default SettingsOrganization;
