import React from 'react'
import {  Grid, TextField, Button, Select, FormControl, MenuItem, InputLabel }  from '@mui/material';
import CityService from "../../services/city.service";

const states = [
           {
              "ID":1,
              "STATE_NAME":"Alabama"
           },
           {
              "ID":2,
              "STATE_NAME":"Alaska"
           },
           {
              "ID":3,
              "STATE_NAME":"Arizona"
           },
           {
              "ID":4,
              "STATE_NAME":"Arkansas"
           },
           {
              "ID":5,
              "STATE_NAME":"California"
           },
           {
              "ID":6,
              "STATE_NAME":"Colorado"
           },
           {
              "ID":7,
              "STATE_NAME":"Connecticut"
           },
           {
              "ID":8,
              "STATE_NAME":"Delaware"
           },
           {
              "ID":9,
              "STATE_NAME":"District of Columbia"
           },
           {
              "ID":10,
              "STATE_NAME":"Florida"
           },
           {
              "ID":11,
              "STATE_NAME":"Georgia"
           },
           {
              "ID":12,
              "STATE_NAME":"Hawaii"
           },
           {
              "ID":13,
              "STATE_NAME":"Idaho"
           },
           {
              "ID":14,
              "STATE_NAME":"Illinois"
           },
           {
              "ID":15,
              "STATE_NAME":"Indiana"
           },
           {
              "ID":16,
              "STATE_NAME":"Iowa"
           },
           {
              "ID":17,
              "STATE_NAME":"Kansas"
           },
           {
              "ID":18,
              "STATE_NAME":"Kentucky"
           },
           {
              "ID":19,
              "STATE_NAME":"Louisiana"
           },
           {
              "ID":20,
              "STATE_NAME":"Maine"
           },
           {
              "ID":21,
              "STATE_NAME":"Maryland"
           },
           {
              "ID":22,
              "STATE_NAME":"Massachusetts"
           },
           {
              "ID":23,
              "STATE_NAME":"Michigan"
           },
           {
              "ID":24,
              "STATE_NAME":"Minnesota"
           },
           {
              "ID":25,
              "STATE_NAME":"Mississippi"
           },
           {
              "ID":26,
              "STATE_NAME":"Missouri"
           },
           {
              "ID":27,
              "STATE_NAME":"Montana"
           },
           {
              "ID":28,
              "STATE_NAME":"Nebraska"
           },
           {
              "ID":29,
              "STATE_NAME":"Nevada"
           },
           {
              "ID":30,
              "STATE_NAME":"New Hampshire"
           },
           {
              "ID":31,
              "STATE_NAME":"New Jersey"
           },
           {
              "ID":32,
              "STATE_NAME":"New Mexico"
           },
           {
              "ID":33,
              "STATE_NAME":"New York"
           },
           {
              "ID":34,
              "STATE_NAME":"North Carolina"
           },
           {
              "ID":35,
              "STATE_NAME":"North Dakota"
           },
           {
              "ID":36,
              "STATE_NAME":"Ohio"
           },
           {
              "ID":37,
              "STATE_NAME":"Oklahoma"
           },
           {
              "ID":38,
              "STATE_NAME":"Oregon"
           },
           {
              "ID":39,
              "STATE_NAME":"Pennsylvania"
           },
           {
              "ID":40,
              "STATE_NAME":"Puerto Rico"
           },
           {
              "ID":41,
              "STATE_NAME":"Rhode Island"
           },
           {
              "ID":42,
              "STATE_NAME":"South Carolina"
           },
           {
              "ID":43,
              "STATE_NAME":"South Dakota"
           },
           {
              "ID":44,
              "STATE_NAME":"Tennessee"
           },
           {
              "ID":45,
              "STATE_NAME":"Texas"
           },
           {
              "ID":46,
              "STATE_NAME":"Utah"
           },
           {
              "ID":47,
              "STATE_NAME":"Vermont"
           },
           {
              "ID":48,
              "STATE_NAME":"Virginia"
           },
           {
              "ID":49,
              "STATE_NAME":"Washington"
           },
           {
              "ID":50,
              "STATE_NAME":"West Virginia"
           },
           {
              "ID":51,
              "STATE_NAME":"Wisconsin"
           },
           {
              "ID":52,
              "STATE_NAME":"Wyoming"
           }
];

const softs = [
   {
      "id": 1,
      "name": "Spectrum"
   },
   {
      "id": 2,
      "name": "ECMS"
   },
   {
      "id": 3,
      "name": "Vista"
   },
   {
      "id": 4,
      "name": "PowerPurchase"
   },
   {
      "id": 5,
      "name": "TimberlinePurchaseOrder"
   },
   {
      "id": 6,
      "name": "TimberlineAccounting"
   },
   {
      "id": 7,
      "name": "QuickBooks"
   },
   {
      "id": 8,
      "name": "ViewPoint"
   },
   {
      "id": 9,
      "name": "Jonas"
   },
   {
      "id": 10,
      "name": "ComputerEase"
   }
];

const CompanyDetails = ({ nextStep, handleChange, values }) => {
  
  // for continue event listener
  const Continue = e => {
    e.preventDefault();
    nextStep();
  }

  return (
            <form>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                        required
                        placeholder="Company email"
                        label="Organization email"
                        onChange={handleChange('org_email')}
                        defaultValue={values.org_email}
                        autoComplete="org_email"
                        fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                        required
                        placeholder="Primary contact email"
                        label="Primary contact email"
                        onChange={handleChange('org_primary_email')}
                        defaultValue={values.org_primary_email}
                        autoComplete="org_primary_email"
                        fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                        required
                        placeholder="Organization name"
                        label="Organization name"
                        onChange={handleChange('org_name')}
                        defaultValue={values.org_name}
                        autoComplete="org_name"
                        fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                        required
                        placeholder="Address"
                        label="Organization address"
                        onChange={handleChange('org_address')}
                        defaultValue={values.org_address}
                        autoComplete="org_address"
                        fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                    <FormControl fullWidth>
                    <InputLabel id="software-label">Accounting Software</InputLabel>
                    <Select
                        required
                        labelId="software-label"
                        onChange={handleChange('software')}
                        fullWidth
                        label="Accounting Software"
                        >
                         {  softs.map((soft) => (
                        <MenuItem value={soft.id}>{soft.name}</MenuItem>
                         ))}
                        </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                    <FormControl fullWidth>
                    <InputLabel id="state-label">State</InputLabel>
                    <Select
                        required
                        labelId="state-label"
                        onChange={handleChange('state')}
                        fullWidth
                        label="State"
                        >
                         {  states.map((state) => (
                        <MenuItem value={state.ID}>{state.STATE_NAME}</MenuItem>
                         ))}
                        </Select>
                        </FormControl>

                    </Grid>
                    <Grid item xs={12}>
                    <FormControl fullWidth>
                    <InputLabel id="state-label">City</InputLabel>
                    <Select
                        required
                        labelId="city-label"
                        onChange={handleChange('city')}
                        fullWidth
                        label="City"
                        >
                        {  values.cities.map((city) => (
                        <MenuItem value={city.ID}>{city.CITY}</MenuItem>
                        ))}
                        </Select>
                        </FormControl>

                    </Grid>
                    <Grid item xs={12}>
                        <TextField 
                        required
                        placeholder="Postal code"
                        label="Postal code"
                        onChange={handleChange('org_postal_code')}
                        defaultValue={values.org_postal_code}
                        autoComplete="org_postal_code"
                        fullWidth
                        />
                    </Grid>
                </Grid>
                <br />
                
                <Button 
                    onClick={ Continue }
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                >
                    Next
                </Button>
            </form>
            
  )
}

export default CompanyDetails