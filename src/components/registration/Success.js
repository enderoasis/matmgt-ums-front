import React from 'react'

const Success = () => {
  return (
      <><h1>You are done!</h1><h2>We will contact you shortly after reviewing your application and inform you about details.</h2></> 
  )
}

export default Success