import React from 'react'
import { Container, Grid, List, ListItem, ListItemText, Button }  from '@mui/material';

const Confirmation = ({ prevStep, nextStep, values }) => {
  console.log(values);
  const {  org_email, org_name, org_address, org_primary_email, state, city, org_postal_code, software } = values
  const Continue = e => {
    e.preventDefault();
    nextStep();
  }

  const Previous = e => {
    e.preventDefault();
    prevStep();
  }

  return (
        <><List>
          <ListItem>
              <ListItemText primary="Organization email:" secondary={org_email} />
          </ListItem>
          <ListItem>
              <ListItemText primary="Organization name:" secondary={org_name} />
          </ListItem>
          <ListItem>
              <ListItemText primary="Address:" secondary={org_address} />
          </ListItem>
          <ListItem>
              <ListItemText primary="State:" secondary={state} />
          </ListItem>
          <ListItem>
              <ListItemText primary="City:" secondary={city} />
          </ListItem>
          <ListItem>
              <ListItemText primary="Primary contact:" secondary={org_primary_email} />
          </ListItem>
          <ListItem>
              <ListItemText primary="Software package:" secondary={software} />
          </ListItem>
          <ListItem>
              <ListItemText primary="Postal code:" secondary={org_postal_code} />
          </ListItem>
      </List><br /><Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                  <Button
                      onClick={Previous}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                  >
                      Previous
                  </Button>
              </Grid>
              <Grid item xs={12} sm={6}>
                  <Button
                      onClick={Continue}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                  >
                      Confirm & Continue
                  </Button>
              </Grid>
          </Grid></>
  )
}

export default Confirmation