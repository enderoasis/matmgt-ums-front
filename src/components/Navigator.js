import * as React from 'react';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import PeopleIcon from '@mui/icons-material/People';
import SettingsIcon from '@mui/icons-material/Settings';
import CorporateFareIcon from '@mui/icons-material/CorporateFare';
import BadgeIcon from '@mui/icons-material/Badge';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import SecurityIcon from '@mui/icons-material/Security';
import { useLocation } from "react-router-dom";
import {Link} from 'react-router-dom';


 
const categories = [
  {
    id: 'Management',
    children: [
      {
        id: 'Users',
        icon: <PeopleIcon />,
        pathname: '/users'
      },
      { id: 'Organizations', icon: <CorporateFareIcon />, pathname: '/organizations' },
      { id: 'Roles', icon: <BadgeIcon /> , pathname: '/roles'},
      { id: 'Jobs', icon: <WorkOutlineIcon /> , pathname: '/jobs'},
      { id: 'Permissions', icon: <SecurityIcon /> , pathname: '/permissions'},
    ],
  },
  {
    id: 'Personal settings',
    children: [
      { id: 'Account', icon: <SettingsIcon />, pathname: '/settings' },
    ],
  },
];

const item = {
  py: '2px',
  px: 3,
  color: 'rgba(255, 255, 255, 0.7)',
  '&:hover, &:focus': {
    bgcolor: 'rgba(255, 255, 255, 0.08)',
  },
};

// const itemCategory = {
//   boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
//   py: 3.5,
//   px: 3,
// };


export default function Navigator(props) {
  const { ...other } = props;
  const location = useLocation()

  return (
    <Drawer variant="permanent" {...other}>
      <List >
        <Link style={{ color: 'inherit', textDecoration: 'inherit'}} to="/">
        <svg xmlns="http://www.w3.org/2000/svg" width="220" height="50" viewBox="0 0 244.52 74.992">
        <g id="MMS-logo-footer" transform="translate(-163.2 -356.9)">
          <g id="Group_206" data-name="Group 206" transform="translate(243.624 366.356)">
            <g id="Group_203" data-name="Group 203" transform="translate(0.287)">
              <path id="Path_272" data-name="Path 272" d="M249.9,366.8h2.483l3.056,9.361,3.152-9.361h2.483l2.292,13.372h-2.483l-1.433-8.5-2.865,8.5h-2.2l-2.77-8.5-1.433,8.5H247.7Z" transform="translate(-247.7 -366.8)" fill="#fff"/>
              <path id="Path_273" data-name="Path 273" d="M281.853,366.8h2.579l5.158,13.372h-2.674l-1.051-2.77h-5.444l-1.146,2.77H276.6Zm1.337,3.534-1.815,4.585h3.534Z" transform="translate(-248.996 -366.8)" fill="#fff"/>
              <path id="Path_274" data-name="Path 274" d="M302.5,366.8h7.355v2.483h-2.483v10.889h-2.483V369.283H302.5Z" transform="translate(-250.158 -366.8)" fill="#fff"/>
              <path id="Path_275" data-name="Path 275" d="M324.3,366.8h7.259v2.483h-4.776v2.388h4.776v2.483h-4.776v3.534h4.776v2.483H324.3Z" transform="translate(-251.135 -366.8)" fill="#fff"/>
              <path id="Path_276" data-name="Path 276" d="M346.7,366.8h3.152c1.719,0,2.865.191,3.821,1.051a3.822,3.822,0,0,1,1.051,2.77,3.477,3.477,0,0,1-2.579,3.534l3.152,5.922h-2.77l-2.961-5.635h-.286v5.635H346.7Zm2.579,5.253h1.146c1.146,0,1.719-.478,1.719-1.337s-.573-1.337-1.815-1.337h-1.146v2.674Z" transform="translate(-252.14 -366.8)" fill="#fff"/>
              <path id="Path_277" data-name="Path 277" d="M369.2,366.8h2.579v13.372H369.2Z" transform="translate(-253.149 -366.8)" fill="#fff"/>
              <path id="Path_278" data-name="Path 278" d="M390.453,366.8h2.579l5.158,13.372h-2.674l-1.051-2.77h-5.444l-1.146,2.77H385.2Zm1.242,3.534-1.815,4.585h3.534Z" transform="translate(-253.866 -366.8)" fill="#fff"/>
              <path id="Path_279" data-name="Path 279" d="M412.4,366.8h2.579v10.984H418.7v2.388h-6.3Z" transform="translate(-255.086 -366.8)" fill="#fff"/>
            </g>
            <g id="Group_204" data-name="Group 204" transform="translate(0 46.898)">
              <path id="Path_280" data-name="Path 280" d="M249.6,425.834a2.572,2.572,0,0,0,2.2,1.719,1.409,1.409,0,0,0,1.528-1.242,2.119,2.119,0,0,0-.86-1.528c-2.77-2.292-4.394-3.63-4.394-5.54a3.453,3.453,0,0,1,3.63-3.343c1.719,0,2.77.955,3.916,2.1l-1.91,1.719c-.669-.86-1.242-1.337-2.006-1.337-.669,0-1.146.382-1.146.955a1.571,1.571,0,0,0,.764,1.146c2.483,2.2,4.489,3.247,4.489,5.635a3.846,3.846,0,0,1-4.107,3.821,4.589,4.589,0,0,1-4.3-2.865Z" transform="translate(-247.4 -415.9)" fill="#fff"/>
              <path id="Path_281" data-name="Path 281" d="M282.75,422.968a6.926,6.926,0,1,1-6.877-7.068A7.014,7.014,0,0,1,282.75,422.968Zm-11.366,0a4.345,4.345,0,0,0,4.394,4.489,4.487,4.487,0,0,0,4.394-4.585,4.426,4.426,0,0,0-4.394-4.585A4.512,4.512,0,0,0,271.383,422.968Z" transform="translate(-248.364 -415.9)" fill="#fff"/>
              <path id="Path_282" data-name="Path 282" d="M296.7,416.3h6.591v2.483h-4.107v2.388h4.107v2.483h-4.107v6.017H296.7Z" transform="translate(-249.611 -415.918)" fill="#fff"/>
              <path id="Path_283" data-name="Path 283" d="M315.4,416.3h7.355v2.483h-2.483v10.889h-2.579V418.783H315.3V416.3Z" transform="translate(-250.445 -415.918)" fill="#fff"/>
              <path id="Path_284" data-name="Path 284" d="M334.7,416.3h2.483l1.91,8.5,2.388-8.5h2.1l2.388,8.5,1.91-8.5h2.483l-3.057,13.372h-2.483l-2.483-8.6-2.388,8.6h-2.388Z" transform="translate(-251.315 -415.918)" fill="#fff"/>
              <path id="Path_285" data-name="Path 285" d="M367.453,416.3h2.579l5.158,13.372h-2.674l-1.051-2.77h-5.444l-1.146,2.77H362.2Zm1.337,3.534-1.815,4.585h3.534Z" transform="translate(-252.548 -415.918)" fill="#fff"/>
              <path id="Path_286" data-name="Path 286" d="M389.5,416.3h3.152c1.719,0,2.866.191,3.821,1.051a3.822,3.822,0,0,1,1.051,2.77,3.477,3.477,0,0,1-2.579,3.534l3.152,5.922h-2.77l-2.961-5.635h-.287v5.635H389.5V416.3Zm2.579,5.253h1.146c1.146,0,1.719-.478,1.719-1.337s-.573-1.337-1.815-1.337h-1.146v2.674Z" transform="translate(-253.772 -415.918)" fill="#fff"/>
              <path id="Path_287" data-name="Path 287" d="M411.3,416.3h7.259v2.483h-4.776v2.388h4.776v2.483h-4.776v3.534h4.776v2.483H411.3Z" transform="translate(-254.75 -415.918)" fill="#fff"/>
            </g>
            <g id="Group_205" data-name="Group 205" transform="translate(0.287 23.21)">
              <path id="Path_288" data-name="Path 288" d="M249.9,391.5h2.483l3.056,9.361,3.152-9.361h2.483l2.292,13.372h-2.483l-1.433-8.5-2.865,8.5h-2.2l-2.77-8.5-1.433,8.5H247.7Z" transform="translate(-247.7 -391.118)" fill="#fff"/>
              <path id="Path_289" data-name="Path 289" d="M274.158,391.5h2.579l5.158,13.372H279.22l-1.051-2.77h-5.444l-1.146,2.77H269Zm1.337,3.534-1.815,4.585h3.534Z" transform="translate(-248.655 -391.118)" fill="#fff"/>
              <path id="Path_290" data-name="Path 290" d="M288.5,391.5h2.483l5.731,8.787V391.5h2.579v13.372H296.81l-5.731-8.787v8.787H288.5Z" transform="translate(-249.53 -391.118)" fill="#fff"/>
              <path id="Path_291" data-name="Path 291" d="M310.953,391.5h2.579l5.158,13.372h-2.674l-1.051-2.77h-5.444l-1.146,2.77H305.7Zm1.242,3.534-1.815,4.585h3.534Z" transform="translate(-250.301 -391.118)" fill="#fff"/>
              <path id="Path_292" data-name="Path 292" d="M337.645,398.073a6.718,6.718,0,0,1-1.624,4.871,6.269,6.269,0,0,1-5.062,2.2,7.023,7.023,0,1,1-.1-14.041,7.189,7.189,0,0,1,5.731,2.483L334.78,395.4a5.439,5.439,0,0,0-4.012-1.815,4.447,4.447,0,0,0-4.585,4.489,4.63,4.63,0,0,0,4.776,4.585,3.581,3.581,0,0,0,3.63-2.292h-3.916v-2.388h6.973Z" transform="translate(-251.108 -391.1)" fill="#fff"/>
              <path id="Path_293" data-name="Path 293" d="M344.8,391.5h7.259v2.483h-4.776v2.388h4.776v2.483h-4.776v3.534h4.776v2.483H344.8Z" transform="translate(-252.054 -391.118)" fill="#fff"/>
              <path id="Path_294" data-name="Path 294" d="M360.1,391.5h2.483l3.056,9.361,3.152-9.361h2.483l2.292,13.372h-2.483l-1.433-8.5-2.865,8.5h-2.2l-2.77-8.5-1.433,8.5H357.9Z" transform="translate(-252.642 -391.118)" fill="#fff"/>
              <path id="Path_295" data-name="Path 295" d="M380.4,391.5h7.259v2.483h-4.776v2.388h4.776v2.483h-4.776v3.534h4.776v2.483H380.4Z" transform="translate(-253.651 -391.118)" fill="#fff"/>
              <path id="Path_296" data-name="Path 296" d="M394.5,391.5h2.483l5.731,8.787V391.5h2.579v13.372H402.81l-5.731-8.787v8.787H394.5Z" transform="translate(-254.283 -391.118)" fill="#fff"/>
              <path id="Path_297" data-name="Path 297" d="M411.5,391.5h7.355v2.483h-2.483v10.889h-2.483V393.983H411.5Z" transform="translate(-255.046 -391.118)" fill="#fff"/>
            </g>
          </g>
          <g id="Group_211" data-name="Group 211" transform="translate(163.2 356.9)">
            <g id="Group_208" data-name="Group 208" transform="translate(35.134)">
              <g id="Group_131_3_" transform="translate(0.206)">
                <g id="Group_129_3_">
                  <path id="Path_250_4_" d="M200.2,373.233a8.3,8.3,0,0,0,0-16.333Z" transform="translate(-200.2 -356.9)" fill="#6d09e1"/>
                </g>
              </g>
              <g id="Group_207" data-name="Group 207" transform="translate(0 17.288)">
                <path id="Path_298" data-name="Path 298" d="M223.4,386.366a4.043,4.043,0,0,0,.669-.478.094.094,0,0,0,.1-.1c.287.1.382-.1.478-.287.86-1.242,1.624-2.483,2.483-3.725.287-.478.669-1.051.955-1.528a2.2,2.2,0,0,0-.955-3.056,5.4,5.4,0,0,0-2.388-.764,3.58,3.58,0,0,0-1.051-.1.333.333,0,0,1-.191-.1c-.1-.191-.287-.478-.382-.669-.1-.1-.1-.191.1-.287a17.589,17.589,0,0,1,3.152-.287,10.159,10.159,0,0,1,3.821.86,14.414,14.414,0,0,1,2.674,1.433,2.157,2.157,0,0,1,.764,1.051,2.089,2.089,0,0,0,.287.478,10.156,10.156,0,0,0,1.051.86.716.716,0,0,0,.478.1,1.491,1.491,0,0,1,1.242.669c.1.1.1.382.1.478-.1.191-.191.478-.287.669a5.8,5.8,0,0,1-.669.955c-.287.478-.573.478-1.051.287a1.874,1.874,0,0,1-.86-.86c-.1-.191-.287-.287-.382-.478a2.683,2.683,0,0,0-1.91-.573c-.382,0-.669.286-1.051.478a8.689,8.689,0,0,0-1.719,2.006c-.86,1.242-1.528,2.388-2.292,3.63a.716.716,0,0,0-.1.478.928.928,0,0,1-.1.86,1.5,1.5,0,0,0-.191,1.528,4.557,4.557,0,0,1-3.725,5.253c-.287.1-.382.1-.478.286-1.242,1.719-2.292,3.63-3.343,5.444-.191.382-.382.669-.573,1.051-.1.191-.287.286-.382.478a1.3,1.3,0,0,1-1.528.191c-.669-.382-1.242-.86-1.91-1.242a1.151,1.151,0,0,1-.382-1.624l.86-1.146c.86-1.242,1.719-2.483,2.674-3.725a6.732,6.732,0,0,1,.669-.86c.1-.191,0-.286-.1-.286-.86-.478-1.719-1.051-2.674-1.528-1.528-.86-3.056-1.815-4.585-2.674-1.051-.573-1.91-1.146-2.961-1.719a.351.351,0,0,0-.287-.1V404.9c0,.191.1.287.1.478,1.242,2.1,2.483,4.394,3.821,6.495.86,1.528,1.719,3.057,2.674,4.585.478.86.86,1.528,1.433,2.388.669,1.146,1.337,2.2,1.91,3.248.86,1.337,1.528,2.674,2.292,4.107a4.143,4.143,0,0,1,.478,3.343,4.651,4.651,0,0,1-6.591,2.579,4.6,4.6,0,0,1-1.91-1.91c-.86-1.433-1.719-2.865-2.483-4.3-.955-1.528-1.91-3.152-2.77-4.68-.86-1.433-1.624-2.77-2.483-4.2-.573-1.051-1.242-2.006-1.719-3.056-.478-.86-.955-1.624-1.433-2.483a5.835,5.835,0,0,1-.764-2.865V381.208a4.362,4.362,0,0,1,.191-1.91,3.618,3.618,0,0,1,1.719-2.2,4.522,4.522,0,0,1,4.394-.382c.573.287,1.051.573,1.528.86,1.624.955,3.343,1.91,4.967,2.865,2.483,1.433,4.871,2.865,7.355,4.394A11.325,11.325,0,0,1,223.4,386.366Z" transform="translate(-199.984 -375)" fill="#6d09e1"/>
              </g>
            </g>
            <g id="Group_210" data-name="Group 210">
              <g id="Group_131_2_" transform="translate(25.549)">
                <g id="Group_129_2_">
                  <path id="Path_250_3_" d="M196.779,373.233a8.3,8.3,0,0,1,0-16.333Z" transform="translate(-189.948 -356.9)" fill="#00d600"/>
                </g>
              </g>
              <g id="Group_209" data-name="Group 209" transform="translate(0 18.898)">
                <path id="Path_299" data-name="Path 299" d="M188.13,387.684c-.573.382-1.146.669-1.719,1.051-3.343,1.91-6.782,4.012-10.125,5.922-.287.191-.382.191-.287.573.573,2.865,1.051,5.731,1.624,8.6,0,.1.1.191,0,.287-1.433-2.1-2.961-4.394-4.394-6.5-1.242,1.051-2.388,1.91-3.63,2.961-.478-4.107-.86-8.214-1.337-12.322l-3.725,3.725c-.478-4.394-.86-8.6-1.337-12.895h.1c1.337,2.292,2.674,4.489,4.107,6.782l3.439-4.585h.1c.191,2.006.382,4.012.573,6.113a.351.351,0,0,1,.287-.1c2.674-1.528,5.253-3.152,7.928-4.68,3.056-1.815,6.208-3.63,9.265-5.444a4.348,4.348,0,0,1,1.91-.478,4.231,4.231,0,0,1,3.152,1.051,3.89,3.89,0,0,1,1.433,3.057v28.559a4.947,4.947,0,0,1-.86,2.674c-3.057,5.062-6.017,10.22-8.978,15.283-.669,1.051-1.242,2.1-1.815,3.056a4.8,4.8,0,0,1-2.961,2.292,4.648,4.648,0,0,1-5.349-3.248,3.874,3.874,0,0,1,.669-3.248c2.674-4.585,5.253-9.169,7.928-13.659,1.433-2.292,2.674-4.68,4.107-7.068a.716.716,0,0,0,.1-.478V388.161C188.13,387.875,188.13,387.779,188.13,387.684Z" transform="translate(-163.2 -376.685)" fill="#00d600"/>
              </g>
            </g>
          </g>
        </g>
      </svg>

        </Link>

        {categories.map(({ id, children}) => (
          <Box style={{marginTop: 5 + 'px'}} key={id} sx={{ bgcolor: '#101F33' }}>

            <ListItem sx={{ py: 2, px: 3 }}>
               <ListItemText sx={{ color: '#fff' }}>{id}</ListItemText> 
            </ListItem>
            {children.map(({ id: childId, icon, active, pathname  }) => (
                  <Link  key={childId} style={{ color: 'inherit', textDecoration: 'inherit'}} to={pathname}>
                    <ListItemButton selected={pathname === location.pathname} sx={item}>
                        <ListItemIcon>{icon}</ListItemIcon>
                        <ListItemText>{childId}</ListItemText>
                    </ListItemButton>
                  </Link>
            ))}

            <Divider sx={{ mt: 2 }} />
          </Box>
        ))}
      </List>
    </Drawer>
  );
}