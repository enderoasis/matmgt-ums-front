class TokenService {
   
    getLocalAccessToken() {
      const user = JSON.parse(localStorage.getItem("user"));
      return user?.access_token;
    }

    getUser() {
      return JSON.parse(localStorage.getItem("user"));
    }

    setUser(user) {
      console.log(JSON.stringify(user));
      localStorage.setItem("user", JSON.stringify(user));
      localStorage.setItem("logged_in", true);

    }

    removeUser() {
      localStorage.removeItem("user");
      localStorage.removeItem("logged_in");
    }
  }
  export default new TokenService();