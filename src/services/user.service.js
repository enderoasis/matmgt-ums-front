import api from './api';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

  class UserService {
    getUsers() {
      return api.get('/v1/users');
    }
    getExactUser(user_id) {
      return api.get('/v1/users/' + user_id);
    }

    updateUser(data,user_id) {
      return api
      .patch("/v1/users/" + user_id, data)
      .then(response => {
      });
  }
  
  uploadUserAvatar(data) {
    return api
      .post("/v1/users/avatar", data)
      .then(response => {
          if(response.status == 201){
            toast.success('Avatar has been uploaded', {
              position: "top-center",
              autoClose: 2000,
            })
          }
      }).then(() => {
        setTimeout(() => { window.location.reload(); }, 1000);
      });
  }
}
export default new UserService();