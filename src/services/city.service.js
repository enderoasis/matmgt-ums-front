import api from "./api";

class CityService {
    getCitiesByState(id) {
      return api.get('/cities/state/' + id);
    }

    getStates() {
      return api.get('/states');
    }
  }
  export default new CityService();