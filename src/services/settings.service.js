import api from "./api";

class SettingsService {
    getSoftwares() {
      return api.get('/v1/softwares');
    }

    getOrgSettings(org_id) {
        return api.get('/v1/organization/settings/' + org_id);
    }

    updateOrgSettings(data,org_id) {
        return api
        .patch("/v1/organization/settings/" + org_id, data)
        .then(response => {
            window.location.href = "/organizations";
        });
    }
  }
  export default new SettingsService();