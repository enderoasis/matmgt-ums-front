import api from "./api";
import { toast } from 'react-toastify';

class OrgService {
    create(data) {
        return api
        .post("/v1/orgs", data)
        .then(response => {
            toast.success('Organization created!', {
              position: "top-center",
              autoClose: 2000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            })
            }).then(() => {
              setTimeout(() => { window.location.href = "/organizations"; }, 2000);
            }        
          );    
    }

    update(data,org_id) {
      return api
      .post("/v1/orgs/" + org_id, data)
      .then(response => {
          toast.success('Organization updated!', {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
      }).then(() => {
        setTimeout(() => { window.location.href = "/organizations"; }, 1000);
      });
    }
  
    getOrgs() {
      return api.get('/v1/orgs');
    }

    getBranches(org_id) {
      return api.get('/v1/branches/organization/' + org_id);
    }

 
  }
  export default new OrgService();