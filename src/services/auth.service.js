import api from "./api";
import TokenService from "./token.service";
import { toast } from 'react-toastify';

class AuthService {

    login(email, password) {
    return api
      .post("/login", {
        email,
        password
      })
      .then(response => {
        if(response.status == 200){
          if (response.data.access_token) {
            TokenService.setUser(response.data);
            window.location.href = "/";
          }
        }
      }).catch((error) => {
          toast.error(error.response.data.response, {
            position: "top-left",
            autoClose: 2000,
          })
      });
  }

  register(result) {
    console.log(result);
    return api
      .post("/v1/users", result)
      .then(response => {
          window.location.href = "/users";
      });
  }

  update(result,user_id) {
    return api
    .patch("/v1/users/" + user_id, result)
    .then(response => {
        window.location.href = "/users";
    });
  }

  logout() {
    TokenService.removeUser();
    window.location.href = "/login";
  }

  getCurrentUser() {
    return TokenService.getUser();
  }

  isLoggedIn() {
      return localStorage.getItem("logged_in") !== null;
  }
}
export default new AuthService();