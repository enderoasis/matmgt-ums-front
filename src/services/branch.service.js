import api from "./api";
import { toast } from 'react-toastify';

class BranchService {
    create(data) {
        return api
        .post("/v1/branches/organization", data)
        .then(response => {
          toast.success('Branch created!', {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
      }).then(() => {
        setTimeout(() => { window.location.href = "/organizations"; }, 1000);
      });  
    }

    update(result,org_id) {
      return api
      .post("/v1/branches/organization/" + org_id, result)
      .then(response => {
        toast.success('Branch updated!', {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
    }).then(() => {
      setTimeout(() => { window.location.href = "/organizations"; }, 1000);
    });
    }
    // Get organization`s branches
    index(org_id) {
      return api.get('/v1/branches/organization/' + org_id);
    }
  }
  export default new BranchService();